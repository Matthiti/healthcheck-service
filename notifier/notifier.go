package notifier

type Notifier interface {
	Notify(url string, healthError error) error
}
