package notifier

import (
	"bytes"
	"fmt"
	"net/http"
	"io"
	"strings"
)

type SlackNotifier struct {
	webhook string
}

func NewSlackNotifier(webhook string) *SlackNotifier {
	return &SlackNotifier{
		webhook: webhook,
	}
}

func (sl SlackNotifier) Notify(url string, healthError error) error {
	jsonData := []byte(fmt.Sprintf(`{
		"text": "Healthcheck failed for URL %s: %s"
	}`, url, strings.ReplaceAll(healthError.Error(), "\"", "\\\"")))

	res, err := http.Post(sl.webhook, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return fmt.Errorf("could not send health error to Slack: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		errMsg := fmt.Sprintf("could not send health error to Slack: received status code %d", res.StatusCode)
		body, err := io.ReadAll(res.Body)
		if err != nil {
			return fmt.Errorf("%s and could not read body", errMsg)
		}
		return fmt.Errorf("%s with body '%s'", errMsg, string(body))
	}
	return nil
}
