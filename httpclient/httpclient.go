package httpclient

import (
	"fmt"
	"net/http"
)

func CheckHealth(url string) error {
	res, err := http.Get(url)
	if err != nil {
		return err
	}

	// Check the status code
	if res.StatusCode >= 400 {
		return fmt.Errorf("received status code %d", res.StatusCode)
	}
	return nil
}
