package main

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"healthcheck-service/daemon"
	"healthcheck-service/notifier"
	"strings"
)

var rootCmd = &cobra.Command{
	Use:   "healthcheck-service",
	Short: "Run the healthcheck service",
	Long:  "Run the healthcheck service",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		urls := viper.GetString("urls")
		if urls == "" {
			logrus.Fatal("No urls provided")
		}

		slackWebhook := viper.GetString("slack.webhook")
		if slackWebhook == "" {
			logrus.Fatal("No Slack webhook provided")
		}

		d := daemon.New(strings.Split(urls, ","), notifier.NewSlackNotifier(slackWebhook))
		d.Start()
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		logrus.Fatalf("Running the root command failed: %v", err)
	}
}

func init() {
	viper.AutomaticEnv()
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		logrus.Warnf("Could not read .env file: %v", err)
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	rootCmd.PersistentFlags().StringP("urls", "u", "", "A comma-separated list of urls to check")
	_ = viper.BindPFlag("urls", rootCmd.PersistentFlags().Lookup("urls"))

	rootCmd.PersistentFlags().StringP("webhook", "w", "", "The Slack webhook to notify")
	_ = viper.BindPFlag("slack.webhook", rootCmd.PersistentFlags().Lookup("webhook"))

	setDefaults()
}

func setDefaults() {
	viper.SetDefault("checker.interval", 1800)
}
