package daemon

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"golang.org/x/exp/slices"
	"healthcheck-service/httpclient"
	"healthcheck-service/notifier"
	"sync"
	"time"
)

type Daemon struct {
	urls           []string
	errorUrls      []string
	errorUrlsMutex sync.Mutex
	notifier       notifier.Notifier
}

func New(urls []string, notifier notifier.Notifier) *Daemon {
	return &Daemon{
		urls:     urls,
		notifier: notifier,
	}
}

func (d *Daemon) Start() {
	interval := viper.GetUint("checker.interval")
	logrus.Infof("Checking health every %d seconds", interval)

	for {
		for i := range d.urls {
			go d.checkUrl(d.urls[i])
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}

func (d *Daemon) checkUrl(url string) {
	err := httpclient.CheckHealth(url)

	d.errorUrlsMutex.Lock()
	defer d.errorUrlsMutex.Unlock()

	if err != nil {
		logrus.Warnf("URL %s not healthy: %v", url, err)
		if !slices.Contains(d.errorUrls, url) {
			d.errorUrls = append(d.errorUrls, url)
			if err = d.notifier.Notify(url, err); err != nil {
				logrus.Errorf("Could not notify: %v", err)
			}
		}
	} else {
		logrus.Infof("URL %s healthy", url)
		idx := slices.Index(d.errorUrls, url)
		if idx != -1 {
			// The health check succeeded, so we remove it from the error urls slice
			d.errorUrls = append(d.errorUrls[:idx], d.errorUrls[idx+1:]...)
		}
	}
}
